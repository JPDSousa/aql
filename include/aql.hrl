-define(AQL_INTEGER, integer).
-define(CRDT_INTEGER, antidote_crdt_integer).

-define(AQL_VARCHAR, varchar).
-define(CRDT_VARCHAR, antidote_crdt_lwwreg).

-define(AQL_BOOLEAN, boolean).
-define(CRDT_BOOLEAN, antidote_crdt_flag_ew).

-define(AQL_COUNTER_INT, counter_int).
-define(CRDT_COUNTER_INT, antidote_crdt_bcounter).
